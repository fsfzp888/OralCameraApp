package com.zpfeng.oralcamera.view;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.media.projection.MediaProjectionManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.clj.fastble.BleManager;
import com.clj.fastble.callback.BleGattCallback;
import com.clj.fastble.callback.BleNotifyCallback;
import com.clj.fastble.callback.BleScanCallback;
import com.clj.fastble.callback.BleWriteCallback;
import com.clj.fastble.data.BleDevice;
import com.clj.fastble.exception.BleException;
import com.clj.fastble.scan.BleScanRuleConfig;
import com.clj.fastble.utils.HexUtil;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.zpfeng.oralcamera.R;
import com.zpfeng.oralcamera.application.OralCameraApplication;
import com.zpfeng.oralcamera.service.ScreenRecordService;
import com.zpfeng.oralcamera.utils.ASCIIUtil;
import com.zpfeng.oralcamera.utils.ChartBase;
import com.zpfeng.oralcamera.utils.DLCKBtCmdSet;
import com.zpfeng.oralcamera.utils.DLCKBtDataPkg;
import com.zpfeng.oralcamera.utils.comm.ObserverManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RespiratoryWaveformsActivity extends ChartBase implements OnChartValueSelectedListener {
    private static final String TAG = RespiratoryWaveformsActivity.class.getSimpleName();
    private static final int REQUEST_CODE_OPEN_GPS = 1;
    private static final int REQUEST_CODE_PERMISSION_LOCATION = 2;
    public LineChart waveform_chart;
    public XAxis mTopAxis;
    public YAxis mLeftAxis;
    public List<BleDevice> bleDeviceList;
    public Map<BluetoothGattService, List<BluetoothGattCharacteristic>> bleServiceMap;
    public DLCKBtCmdSet deviceCmdSet;
    private BleDevice connectBleDevice;
    private BluetoothGattCharacteristic writeCharacteristic;
    private BluetoothGattCharacteristic notifyCharacteristic;
    private int[] colorArray;
    private String[] titleArray;
    private int mXRangeMaximum = 60;
    private int minAxisY = -1;
    private int maxAxisY = 50;
    AlertDialog mAxisRangeSetDialog;

    enum CaptureMode {
        PRESSURE_MODE, MEASURE_MODE
    }

    CaptureMode mCaptureMode = CaptureMode.PRESSURE_MODE;


    private int mCaptureScreenWidth;
    private int mCaptureScreenHeight;
    private int mCaptureScreenDensity;

    private boolean isStarted = false;
    private boolean isVideoSd = false;
    private boolean isAudio = true;

    private static final String RECORD_STATUS = "record_status";
    private static final int REQUEST_CODE = 1000;

    private void getScreenBaseInfo() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mCaptureScreenWidth = metrics.widthPixels;
        mCaptureScreenHeight = metrics.heightPixels;
        mCaptureScreenDensity = metrics.densityDpi;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);
        outState.putBoolean(RECORD_STATUS, isStarted);
    }

    private void startScreenRecording() {
        // TODO Auto-generated method stub
        MediaProjectionManager mediaProjectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        assert mediaProjectionManager != null;
        Intent permissionIntent = mediaProjectionManager.createScreenCaptureIntent();
        startActivityForResult(permissionIntent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // 获得权限，启动Service开始录制
                Intent service = new Intent(this, ScreenRecordService.class);
                service.putExtra("code", resultCode);
                service.putExtra("data", data);
                service.putExtra("audio", isAudio);
                service.putExtra("width", mCaptureScreenWidth);
                service.putExtra("height", mCaptureScreenHeight);
                service.putExtra("density", mCaptureScreenDensity);
                service.putExtra("quality", isVideoSd);
                service.putExtra("file_name", getFilename());
                startService(service);
                // 已经开始屏幕录制，修改UI状态
                isStarted = !isStarted;
                Log.i(TAG, "Started screen recording");
            } else {
                Toast.makeText(this, R.string.user_cancelled, Toast.LENGTH_LONG).show();
                Log.i(TAG, "User cancelled");
            }
        }
    }

    /**
     * 关闭屏幕录制，即停止录制Service
     */
    private void stopScreenRecording() {
        // TODO Auto-generated method stub
        Intent service = new Intent(this, ScreenRecordService.class);
        stopService(service);
        isStarted = !isStarted;
        Toast.makeText(this, R.string.capture_waveform_video_finish, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // 在这里将BACK键模拟了HOME键的返回桌面功能（并无必要）
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_realtime_linechart);
        setTitle(getResources().getString(R.string.respiratory_waveform));
        waveform_chart = findViewById(R.id.waveform_chart);
        waveform_chart.setOnChartValueSelectedListener(this);
        // enable description text
        waveform_chart.getDescription().setEnabled(true);
        waveform_chart.getDescription().setText(getResources().getString(R.string.respiratory_waveform));
        // enable touch gestures
        waveform_chart.setTouchEnabled(true);
        // enable scaling and dragging
        waveform_chart.setDragEnabled(true);
        waveform_chart.setScaleEnabled(true);
        waveform_chart.setDrawGridBackground(true);
        // if disabled, scaling can be done on x- and y-axis separately
        waveform_chart.setPinchZoom(true);
        // set an alternative background color
        waveform_chart.setBackgroundColor(Color.LTGRAY);
        LineData data = new LineData();
        data.setValueTextColor(Color.RED);
        // add empty data
        waveform_chart.setData(data);
        // get the legend (only possible after setting data)
        Legend l = waveform_chart.getLegend();
        // modify the legend ...
        l.setForm(LegendForm.LINE);
        l.setTypeface(tfLight);
        l.setTextColor(Color.WHITE);
        mTopAxis = waveform_chart.getXAxis();
        mTopAxis.setTypeface(tfLight);
        mTopAxis.setTextColor(Color.BLACK);
        mTopAxis.setDrawGridLines(true);
        mTopAxis.setAvoidFirstLastClipping(true);
        mTopAxis.setEnabled(true);
        mLeftAxis = waveform_chart.getAxisLeft();
        mLeftAxis.setTypeface(tfLight);
        mLeftAxis.setTextColor(Color.BLACK);
        mLeftAxis.setAxisMaximum(maxAxisY);
        mLeftAxis.setAxisMinimum(minAxisY);
        mLeftAxis.setDrawGridLines(true);
        YAxis rightAxis = waveform_chart.getAxisRight();
        rightAxis.setEnabled(false);
        initView();

        BleManager.getInstance().init(getApplication());
        BleManager.getInstance()
                .enableLog(true)
                .setReConnectCount(1, 5000)
                .setConnectOverTime(20000)
                .setOperateTimeout(5000);
        getScreenBaseInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disconnect();
        BleManager.getInstance().destroy();
        if (isStarted) {
            stopScreenRecording();
        }
    }

    public void addAndConnectDevice(BleDevice bleDevice) {
        for (int i = 0; i < bleDeviceList.size(); i++) {
            BleDevice device = bleDeviceList.get(i);
            if (bleDevice.getKey().equals(device.getKey())) {
                return;
            }
        }
        bleDeviceList.add(bleDevice);
        for (int i = 0; i < bleDeviceList.size(); i++) {
            BleDevice device = bleDeviceList.get(i);
            if (!BleManager.getInstance().isConnected(bleDevice)) {
                connect(bleDevice);
                return;
            }
        }
    }

    public void removeDevice(BleDevice bleDevice) {
        for (int i = 0; i < bleDeviceList.size(); i++) {
            BleDevice device = bleDeviceList.get(i);
            if (bleDevice.getKey().equals(device.getKey())) {
                bleDeviceList.remove(i);
            }
        }
    }

    private void writeCharacteristicData(BluetoothGattCharacteristic characteristic, String data, BleWriteCallback callback) {
        if (connectBleDevice != null && characteristic != null) {
            BleManager.getInstance().write(
                    connectBleDevice,
                    characteristic.getService().getUuid().toString(),
                    characteristic.getUuid().toString(),
                    HexUtil.hexStringToBytes(ASCIIUtil.asciiToHex(data)),
                    callback
            );
        }
    }

    private void openNotifyCharacteristic(BluetoothGattCharacteristic characteristic, BleNotifyCallback callback) {
        if (connectBleDevice != null && characteristic != null) {
            BleManager.getInstance().notify(
                    connectBleDevice,
                    characteristic.getService().getUuid().toString(),
                    characteristic.getUuid().toString(),
                    callback
            );
        }
    }

    private void closeNotifyCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (connectBleDevice != null && characteristic != null) {
            BleManager.getInstance().stopNotify(
                    connectBleDevice,
                    characteristic.getService().getUuid().toString(),
                    characteristic.getUuid().toString()
            );
            BleManager.getInstance().removeNotifyCallback(connectBleDevice, characteristic.getUuid().toString());
        }
    }

    private void extractBluetoothCharacteristic() {
        for (BluetoothGattService service : bleServiceMap.keySet()) {
            if (service.getUuid().equals(deviceCmdSet.service_uuid)) {
                List<BluetoothGattCharacteristic> gl = bleServiceMap.get(service);
                if (gl == null) {
                    Toast.makeText(RespiratoryWaveformsActivity.this, "Fail to find valid characteristic", Toast.LENGTH_LONG).show();
                    continue;
                }
                for (BluetoothGattCharacteristic chara : gl) {
                    if (chara.getUuid().equals(deviceCmdSet.write_characteristic_uuid)) {
                        writeCharacteristic = chara;
                    }
                    else if (chara.getUuid().equals(deviceCmdSet.notify_characteristic_uuid)) {
                        notifyCharacteristic = chara;
                    }
                }
            }
        }
    }

    private void swapCaptureMode() {
        if (connectBleDevice == null || writeCharacteristic == null) return;
        writeCharacteristicData(writeCharacteristic, deviceCmdSet.stop_measure, new BleWriteCallback() {
            @Override
            public void onWriteSuccess(int current, int total, byte[] justWrite) {
                Log.i(TAG, "write stop measure command success");
                closeNotifyCharacteristic(notifyCharacteristic);
                waveform_chart.clearValues();
                initDataSet();
                if (mCaptureMode == CaptureMode.PRESSURE_MODE) {
                    mCaptureMode = CaptureMode.MEASURE_MODE;
                } else {
                    mCaptureMode = CaptureMode.PRESSURE_MODE;
                }
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        openNotifyGetDisplayData();
                    }
                }, 500);
            }

            @Override
            public void onWriteFailure(BleException exception) {
                Log.w(TAG, "write stop measure command fail");
                closeNotifyCharacteristic(notifyCharacteristic);
                Toast.makeText(RespiratoryWaveformsActivity.this, "Fail to write stop measure command", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void stopNotifyAndDisconnect() {
        writeCharacteristicData(writeCharacteristic, deviceCmdSet.stop_measure, new BleWriteCallback() {
            @Override
            public void onWriteSuccess(int current, int total, byte[] justWrite) {
                Log.i(TAG, "write stop measure command success");
                closeNotifyCharacteristic(notifyCharacteristic);
                BleManager.getInstance().disconnect(connectBleDevice);
                connectBleDevice = null;
                writeCharacteristic = null;
                notifyCharacteristic = null;
                bleServiceMap.clear();
            }
            @Override
            public void onWriteFailure(BleException exception) {
                Log.w(TAG, "write stop measure command fail");
                closeNotifyCharacteristic(notifyCharacteristic);
                BleManager.getInstance().disconnect(connectBleDevice);
                connectBleDevice = null;
                writeCharacteristic = null;
                notifyCharacteristic = null;
                bleServiceMap.clear();
                Toast.makeText(RespiratoryWaveformsActivity.this, "Fail to write stop measure command", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void openNotifyGetDisplayData() {
        openNotifyCharacteristic(notifyCharacteristic, new BleNotifyCallback() {
            @Override
            public void onNotifySuccess() {
                Log.i(TAG, "open notify success");
                String command;
                if (mCaptureMode == CaptureMode.PRESSURE_MODE) {
                    command = deviceCmdSet.start_pressure_mode;
                } else {
                    command = deviceCmdSet.start_measure_mode;
                }
                writeCharacteristicData(writeCharacteristic, command, new BleWriteCallback() {
                    @Override
                    public void onWriteSuccess(int current, int total, byte[] justWrite) {
                        Log.i(TAG, "write start command success");
                    }

                    @Override
                    public void onWriteFailure(BleException exception) {
                        Log.w(TAG, "write start command fail");
                        Toast.makeText(RespiratoryWaveformsActivity.this, "Fail to write start pressure mode command", Toast.LENGTH_LONG).show();
                    }
                });
            }

            @Override
            public void onNotifyFailure(BleException exception) {
                Log.w(TAG, "open notify fail");
                Toast.makeText(RespiratoryWaveformsActivity.this, "Fail to open notify", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCharacteristicChanged(byte[] data) {
                String data2 = new String(data);
                Log.w(TAG, data2);
                DLCKBtDataPkg pkg = deviceCmdSet.parseData(data2);
                if (pkg != null) {
                    if (mCaptureMode == CaptureMode.PRESSURE_MODE && pkg.getDataType() == DLCKBtDataPkg.DataType.U0) {
                        int pressure_val = pkg.getData(0);
                        addDataValEntry(pressure_val, 1);
                    } else if (mCaptureMode == CaptureMode.MEASURE_MODE && pkg.getDataType() == DLCKBtDataPkg.DataType.U1) {
                        int times_per_min = pkg.getData(0);
                        int period_100ms = pkg.getData(1);
                        int pressure_diff = pkg.getData(2);
                        addDataValEntry(times_per_min, 2);
                        addDataValEntry(period_100ms, 3);
                        addDataValEntry(pressure_diff, 4);
                    }
                }
            }
        });
    }

    private void disconnect() {
        stopNotifyAndDisconnect();
    }

    private void connect(final BleDevice bleDevice) {
        BleManager.getInstance().connect(bleDevice, new BleGattCallback() {
            @Override
            public void onStartConnect() {
                Log.i(TAG, "start connecting device");
                bleServiceMap.clear();
                connectBleDevice = null;
            }

            @Override
            public void onConnectFail(BleDevice bleDevice, BleException exception) {
                Log.i(TAG, "connect device fail");
                Toast.makeText(RespiratoryWaveformsActivity.this, getString(R.string.connect_fail), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onConnectSuccess(BleDevice bleDevice, BluetoothGatt gatt, int status) {
                Log.i(TAG, "connect device success");
                Toast.makeText(RespiratoryWaveformsActivity.this, getString(R.string.connect_success), Toast.LENGTH_LONG).show();
                connectBleDevice = bleDevice;
                for (BluetoothGattService service : gatt.getServices()) {
                    bleServiceMap.put(service, new ArrayList<>());
                    List<BluetoothGattCharacteristic> gl = bleServiceMap.get(service);
                    gl.addAll(service.getCharacteristics());
                }
                extractBluetoothCharacteristic();
                openNotifyGetDisplayData();
            }

            @Override
            public void onDisConnected(boolean isActiveDisConnected, BleDevice bleDevice, BluetoothGatt gatt, int status) {
                Log.i(TAG, "disconnect device");
                stopNotifyAndDisconnect();
                if (isActiveDisConnected) {
                    Toast.makeText(RespiratoryWaveformsActivity.this, getString(R.string.disconnected), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(RespiratoryWaveformsActivity.this, getString(R.string.disconnected), Toast.LENGTH_LONG).show();
                    ObserverManager.getInstance().notifyObserver(bleDevice);
                }
            }
        });
    }

    private void initView() {
        bleDeviceList = new ArrayList<>();
        bleServiceMap = new HashMap<>();
        deviceCmdSet = new DLCKBtCmdSet();
        colorArray = new int[]{Color.LTGRAY, Color.BLUE, Color.RED, Color.BLACK, Color.YELLOW};
        titleArray = new String[]{"Random Test Data", "RealTimePressure(mmHg)", "BreathTimes(times/min)", "period(100ms)", "PressureDiff(mmHg)"};
        initDataSet();
        Toolbar toolbar = findViewById(R.id.waveform_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        //checkBluetoothPermissions();
    }

    private void checkAndConnectDevice() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (!bluetoothAdapter.isEnabled()) {
            Toast.makeText(this, getString(R.string.please_open_blue), Toast.LENGTH_LONG).show();
            return;
        }

        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
        List<String> permissionDeniedList = new ArrayList<>();
        for (String permission : permissions) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, permission);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                onPermissionGranted(permission);
            } else {
                permissionDeniedList.add(permission);
            }
        }
        if (!permissionDeniedList.isEmpty()) {
            String[] deniedPermissions = permissionDeniedList.toArray(new String[permissionDeniedList.size()]);
            ActivityCompat.requestPermissions(this, deniedPermissions, REQUEST_CODE_PERMISSION_LOCATION);
        }
    }

    private void onPermissionGranted(String permission) {
        switch (permission) {
            case Manifest.permission.ACCESS_FINE_LOCATION:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !checkGPSIsOpen()) {
                    new AlertDialog.Builder(this)
                            .setTitle(R.string.notifyTitle)
                            .setMessage(R.string.gpsNotifyMsg)
                            .setNegativeButton(R.string.cancel,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            finish();
                                        }
                                    })
                            .setPositiveButton(R.string.setting,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                            startActivityForResult(intent, REQUEST_CODE_OPEN_GPS);
                                        }
                                    })

                            .setCancelable(false)
                            .show();
                } else {
                    setScanRule();
                    startScan();
                }
                break;
        }
    }

    private boolean checkGPSIsOpen() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (locationManager == null)
            return false;
        return locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
    }

    private void setScanRule() {
        String[] names = {deviceCmdSet.device_name};
        BleScanRuleConfig scanRuleConfig = new BleScanRuleConfig.Builder()
                //.setServiceUuids(serviceUuids)      // 只扫描指定的服务的设备，可选
                .setDeviceName(true, names)   // 只扫描指定广播名的设备，可选
                //.setDeviceMac(mac)                  // 只扫描指定mac的设备，可选
                .setAutoConnect(true)      // 连接时的autoConnect参数，可选，默认false
                .setScanTimeOut(1000)              // 扫描超时时间，可选，默认10秒
                .build();
        BleManager.getInstance().initScanRule(scanRuleConfig);
    }

    public void clearScanDevice() {
        for (int i = 0; i < bleDeviceList.size(); i++) {
            BleDevice device = bleDeviceList.get(i);
            if (!BleManager.getInstance().isConnected(device)) {
                bleDeviceList.remove(i);
            }
        }
    }

    private void startScan() {
        BleManager.getInstance().scan(new BleScanCallback() {
            @Override
            public void onScanStarted(boolean success) {
                Log.i(TAG, "start scanning device");
                Toast.makeText(RespiratoryWaveformsActivity.this, getString(R.string.searching), Toast.LENGTH_LONG).show();
                clearScanDevice();
            }

            @Override
            public void onLeScan(BleDevice bleDevice) {
                super.onLeScan(bleDevice);
            }

            @Override
            public void onScanning(BleDevice bleDevice) {
                Log.i(TAG, "add scanning device");
            }

            @Override
            public void onScanFinished(List<BleDevice> scanResultList) {
                Log.i(TAG, "scan device finish");
                for (int i = 0; i < scanResultList.size(); ++i) {
                    addAndConnectDevice(scanResultList.get(i));
                }
                if (scanResultList.size() == 0) {
                    Toast.makeText(RespiratoryWaveformsActivity.this, getString(R.string.DLCK_device_not_found), Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(RespiratoryWaveformsActivity.this, getString(R.string.connecting_device), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void initDataSet() {
        LineData data = waveform_chart.getData();
        if (data != null) {
            // create 5 data set by default
            for (int i = 0; i < colorArray.length; ++i) {
                ILineDataSet set = data.getDataSetByIndex(i);
                if (set == null) {
                    set = createDataSet(i);
                    data.addDataSet(set);
                }
            }
        }
    }

    private void addDataValEntry(int val, int dataSetIndex) {
        LineData data = waveform_chart.getData();
        if (data != null) {
            data.addEntry(new Entry(data.getEntryCount(), val), dataSetIndex);
            data.notifyDataChanged();
            // let the chart know it's data has changed
            waveform_chart.notifyDataSetChanged();
            // limit the number of visible entries
            waveform_chart.setVisibleXRangeMaximum(mXRangeMaximum);
            // chart.setVisibleYRange(30, AxisDependency.LEFT);
            // move to the latest entry
            waveform_chart.moveViewToX(data.getEntryCount());
        }
    }

    private void addTestEntry() {
        LineData data = waveform_chart.getData();
        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well
            if (set == null) {
                set = createTestDataSet();
                data.addDataSet(set);
            }
            data.addEntry(new Entry(set.getEntryCount(), (float) (Math.random() * 40) + 30f), 0);
            data.notifyDataChanged();
            // let the chart know it's data has changed
            waveform_chart.notifyDataSetChanged();
            // limit the number of visible entries
            waveform_chart.setVisibleXRangeMaximum(mXRangeMaximum);
            // chart.setVisibleYRange(30, AxisDependency.LEFT);
            // move to the latest entry
            waveform_chart.moveViewToX(data.getEntryCount());
            // this automatically refreshes the chart (calls invalidate())
            // chart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);
        }
    }

    private LineDataSet createDataSet(int idx) {
        LineDataSet set = new LineDataSet(null, titleArray[idx]);
        set.setAxisDependency(AxisDependency.LEFT);
        //set.setColor(ColorTemplate.getHoloBlue());
        set.setColor(colorArray[idx]);
        //set.setColor(Color.GRAY);
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        //set.setCircleRadius(4f);
        //set.setFillAlpha(65);
        //set.setFillColor(ColorTemplate.getHoloBlue());
        set.setFillColor(colorArray[idx]);
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.BLACK);
        set.setValueTextSize(9f);
        set.setDrawValues(true);
        return set;
    }

    private LineDataSet createTestDataSet() {
        LineDataSet set = new LineDataSet(null, "Random Test Data");
        set.setAxisDependency(AxisDependency.LEFT);
        set.setColor(ColorTemplate.getHoloBlue());
        //set.setColor(Color.GRAY);
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(4f);
        set.setFillAlpha(65);
        //set.setFillColor(ColorTemplate.getHoloBlue());
        set.setColor(Color.LTGRAY);
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.BLACK);
        set.setValueTextSize(9f);
        set.setDrawValues(true);
        return set;
    }

    private Thread thread;

    private void feedMultiple() {
        if (thread != null)
            thread.interrupt();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                addTestEntry();
            }
        };
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 500; i++) {
                    // Don't generate garbage runnables inside the loop.
                    runOnUiThread(runnable);
                    try {
                        Thread.sleep(125);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.waveform_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_clear_chart: {
                waveform_chart.clearValues();
                Toast.makeText(this, "Chart cleared!", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.action_feed_test_multiple: {
                feedMultiple();
                break;
            }
            case R.id.action_save_waveform: {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    saveToGallery();
                } else {
                    requestStoragePermission(waveform_chart);
                }
                break;
            }
            case R.id.disconnect_ble_device: {
                disconnect();
                break;
            }
            case R.id.search_connect_device: {
                checkAndConnectDevice();
                break;
            }
            case R.id.swap_capture_mode: {
                swapCaptureMode();
                break;
            }
            case R.id.set_axis: {
                showAsixRangeSetDialog();
                break;
            }
            case R.id.capture_waveform_video: {
                if (!isStarted) {
                    startScreenRecording();
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 60000ms
                            stopScreenRecording();
                        }
                    }, 60000);
                }
                Toast.makeText(this, R.string.already_start_capture_waveform_video, Toast.LENGTH_LONG).show();
                break;
            }
            case android.R.id.home: {
                finish();
                break;
            }
        }
        return true;
    }

    private String getFilename() {
        String file_name = getResources().getString(R.string.respiratory_waveform);
        long time = System.currentTimeMillis();
        OralCameraApplication app = (OralCameraApplication) getApplication();
        String patient_name = app.getPatientName();
        file_name = file_name + "-" + patient_name;
        return file_name;
    }

    @Override
    protected void saveToGallery() {
        saveToGallery(waveform_chart, getFilename());
        /*File extBaseDir = Environment.getExternalStorageDirectory();
        // image naming and path  to include sd card  appending name you choose for file
        String mPath = extBaseDir.getAbsolutePath() + "/DCIM/" + getFilename() + "_" + System.currentTimeMillis() + ".jpg";
        // create bitmap screen capture
        Bitmap bitmap;
        View v1 = findViewById(R.id.waveform_chart); // take the view from your webview
        v1.setDrawingCacheEnabled(true);
        bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);

        OutputStream fout = null;
        File imageFile = new File(mPath);

        try {
            fout = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fout);
            fout.flush();
            fout.close();

        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        Log.i("Entry selected", e.toString());
    }

    @Override
    public void onNothingSelected() {
        Log.i("Nothing selected", "Nothing selected.");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (thread != null) {
            thread.interrupt();
        }
    }

    private void showAsixRangeSetDialog() {
        String max_y_val = String.valueOf(maxAxisY);
        String min_y_val = String.valueOf(minAxisY);
        String x_range_val = String.valueOf(mXRangeMaximum);
        View root_view = LayoutInflater.from(RespiratoryWaveformsActivity.this).inflate(R.layout.axis_range_dialog, null);
        EditText et_x_range = root_view.findViewById(R.id.axis_set_x_range);
        EditText et_min_y = root_view.findViewById(R.id.axis_range_set_y_min);
        EditText et_max_y = root_view.findViewById(R.id.axis_range_set_y_max);
        et_x_range.setInputType(InputType.TYPE_CLASS_NUMBER);
        et_x_range.setText(x_range_val.toCharArray(), 0, x_range_val.length());
        et_min_y.setInputType(InputType.TYPE_CLASS_NUMBER);
        et_max_y.setInputType(InputType.TYPE_CLASS_NUMBER);
        et_min_y.setText(min_y_val.toCharArray(), 0, min_y_val.length());
        et_max_y.setText(max_y_val.toCharArray(), 0, max_y_val.length());
        AlertDialog.Builder builder = new AlertDialog.Builder(RespiratoryWaveformsActivity.this);
        Button confirm = root_view.findViewById(R.id.axis_range_set_confirm);
        confirm.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                mXRangeMaximum = Integer.parseInt(et_x_range.getText().toString());
                ;
                minAxisY = Integer.parseInt(et_min_y.getText().toString());
                maxAxisY = Integer.parseInt(et_max_y.getText().toString());
                if (minAxisY > maxAxisY) {
                    int tmp = minAxisY;
                    minAxisY = maxAxisY;
                    maxAxisY = tmp;
                }
                mLeftAxis = waveform_chart.getAxisLeft();
                mLeftAxis.setAxisMaximum(maxAxisY);
                mLeftAxis.setAxisMinimum(minAxisY);
                //waveform_chart.zoomIn();
                //waveform_chart.zoomOut();
                mAxisRangeSetDialog.dismiss();
                waveform_chart.setVisibleXRangeMaximum(mXRangeMaximum);
            }
        });
        mAxisRangeSetDialog = builder.setView(root_view).create();
        mAxisRangeSetDialog.show();
    }
}
