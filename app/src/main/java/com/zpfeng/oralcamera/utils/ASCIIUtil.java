package com.zpfeng.oralcamera.utils;

public class ASCIIUtil {
    public static String asciiToHex(String ascii) {
        StringBuilder hex = new StringBuilder();
        for (int i = 0; i < ascii.length(); ++i) {
            int ch = ascii.charAt(i);
            String sc = Integer.toHexString(ch);
            if (sc.length() == 1) hex.append("0");
            hex.append(sc);
        }
        return hex.toString();
    }

    public static String hexToAscii(String s) {
        if (s == null || s.equals("")) return null;
        s = s.replace(" ", "");
        byte[] keyword = new byte[s.length() / 2];
        for (int i = 0; i < keyword.length; ++i) {
            try {
                keyword[i] = (byte) (0xff & Integer.parseInt(s.substring(i * 2, i * 2 + 1), 16));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            s = new String(keyword);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }
}
