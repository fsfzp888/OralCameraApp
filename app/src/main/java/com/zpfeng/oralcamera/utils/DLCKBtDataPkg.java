package com.zpfeng.oralcamera.utils;

public class DLCKBtDataPkg {
    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public int[] getDataArray() {
        return dataArray;
    }

    public int getData(int idx) { return dataArray[idx];}

    public void setDataArray(int[] dataArray) {
        this.dataArray = dataArray;
    }

    public enum DataType {
        U0, U1, U2
    }

    private DataType dataType;
    private int[] dataArray;

    public DLCKBtDataPkg(DataType dt, int[] dtArray) {
        this.dataType = dt;
        this.dataArray = dtArray;
    }

}
