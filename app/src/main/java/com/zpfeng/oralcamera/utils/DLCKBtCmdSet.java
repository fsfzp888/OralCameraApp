package com.zpfeng.oralcamera.utils;

import java.util.UUID;

public class DLCKBtCmdSet {
    public final String device_name = "DLCKM01";
    public final String hardware_version = "AT+HV\r\n";
    public final String software_version = "AT+SV\r\n";
    public final String stop_measure = "AT+ST:0\r\n";
    public final String start_pressure_mode = "AT+ST:1\r\n";
    public final String start_measure_mode = "AT+ST:2\r\n";
    public final String sleep = "AT+SLEEP\r\n";
    public final String query_baud_rate = "AT+BD\r\n";
    public final String query_breath_times = "AT+RS\r\n";
    public final UUID service_uuid = UUID.fromString("0000aaf0-0000-1000-8000-00805f9b34fb");
    public final UUID write_characteristic_uuid = UUID.fromString("0000aaf1-0000-1000-8000-00805f9b34fb");
    public final UUID notify_characteristic_uuid = UUID.fromString("0000aaf2-0000-1000-8000-00805f9b34fb");

    public String parseHardwareVersion(String data) {
        String[] d = data.split(":");
        if (d.length == 2 && d[0].equals("AT+HV")) {
            return d[1].substring(0, d[1].length() - 2);
        }
        return data;
    }

    public String parseSoftwareVersion(String data) {
        String[] d = data.split(":");
        if (d.length == 2 && d[0].equals("AT+SV")) {
            return d[1].substring(0, d[1].length() - 2);
        }
        return data;
    }

    public int parseBaudRate(String data) {
        String[] d = data.split(":");
        if (d.length == 2 && d[0].equals("AT+BD")) {
            try {
                return Integer.parseInt(d[1].substring(0, d[1].length() - 2));
            } catch (Exception e) {
                return 9600;
            }
        }
        return 9600;
    }

    public DLCKBtDataPkg parseData(String data) {
        String[] d = data.split(":");
        if (d.length == 2) {
            int[] arr = new int[3];
            switch (d[0]) {
                case "U0": {
                    arr[0] = Integer.parseInt(d[1].substring(0, d[1].length() - 2));
                    return new DLCKBtDataPkg(DLCKBtDataPkg.DataType.U0, arr);
                }
                case "U1": {
                    String[] dd = d[1].substring(0, d[1].length() - 2).split(",");
                    if (dd.length < 3) return null;
                    arr[0] = Integer.parseInt(dd[0]);
                    arr[1] = Integer.parseInt(dd[1]);
                    arr[2] = Integer.parseInt(dd[2]);
                    return new DLCKBtDataPkg(DLCKBtDataPkg.DataType.U1, arr);
                }
                case "U2": {
                    String[] dd = d[1].substring(0, d[1].length() - 2).split(",");
                    if (dd.length < 2) return null;
                    arr[0] = Integer.parseInt(dd[0]);
                    arr[1] = Integer.parseInt(dd[1]);
                    return new DLCKBtDataPkg(DLCKBtDataPkg.DataType.U2, arr);
                }
                default:
                    return null;
            }
        }
        return null;
    }
}
