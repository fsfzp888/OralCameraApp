package com.zpfeng.oralcamera.application;

import android.app.Application;

import com.zpfeng.oralcamera.utils.CrashHandler;

public class OralCameraApplication extends Application {
    private CrashHandler mCrashHandler;
    // File Directory in sd card
    public static final String DIRECTORY_NAME = "USBCamera";
    private String patientName = "";
    private String patientTemperature = "";

    @Override
    public void onCreate() {
        super.onCreate();
        mCrashHandler = CrashHandler.getInstance();
        mCrashHandler.init(getApplicationContext(), getClass());
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientTemperature() {
        return patientTemperature;
    }

    public void setPatientTemperature(String patientTemperature) {
        this.patientTemperature = patientTemperature;
    }
}
