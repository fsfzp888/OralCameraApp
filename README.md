# 多功能生命体征数据检测app

本app是为佛山市顺德区陈登职业技术学校创客实验室定制修改的一个外包安卓app，支持使用该校创客实验室开发的各类多功能生命体征数据检测设备，可供该校师生学习参考

## 功能
1. 外置USB摄像头拍照
2. 连拍
3. 对焦
4. 设置分辨率
5. 调整亮度对比度
6. 设置连拍间隔
7. 相册分享功能
8. 设置连拍间隔
9. 摄像头硬件按钮点击直接连拍三张
10. 蓝牙BLE连接测试
11. 蓝牙连接DLCKM01呼吸模块

## apk下载链接
[https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/release/app-release.apk](https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/release/app-release.apk)

## 支持的安卓版本
Android 5.0+

## 示例界面
### 主界面
一个640x480的USB WebCam:
![](https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/capture_view.jpg)
### 预览和分享
![](https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/preview_picture1.jpg)
![](https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/preview_picture2.jpg)

## 呼吸模块
蓝牙BLE使用GATT协议进行通信，GATT协议包含:
1. Profile
2. Service
3. Characteristic

蓝牙呼吸模块DLCKM01使用的命令集：
``` java
public class DLCKBtCmdSet {
    public final String device_name = "DLCKM01"; // 过滤的蓝牙设备名
    public final String hardware_version = "AT+HV\r\n"; // 获取硬件版本
    public final String software_version = "AT+SV\r\n"; // 获取软件版本
    public final String stop_measure = "AT+ST:0\r\n"; // 停止测量
    public final String start_pressure_mode = "AT+ST:1\r\n"; // 压力测量
    public final String start_measure_mode = "AT+ST:2\r\n"; // 计算测量
    public final String sleep = "AT+SLEEP\r\n";
    public final String query_baud_rate = "AT+BD\r\n";
    public final String query_breath_times = "AT+RS\r\n";
    // 蓝牙服务UUID
    public final UUID service_uuid = UUID.fromString("0000aaf0-0000-1000-8000-00805f9b34fb");
    // 读写命令UUID
    public final UUID write_characteristic_uuid = UUID.fromString("0000aaf1-0000-1000-8000-00805f9b34fb");
    // 通知UUID
    public final UUID notify_characteristic_uuid = UUID.fromString("0000aaf2-0000-1000-8000-00805f9b34fb");
}
```

![](https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/preview2.jpg)
![](https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/wav1.jpg)
![](https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/wav2.png)
![](https://gitee.com/fsfzp888/OralCameraApp/raw/master/release/wav3.png)


## 参考
1. [saki4510t/UVCCamera](https://github.com/saki4510t/UVCCamera)
2. [Jasonchenlijian/FastBle](https://github.com/Jasonchenlijian/FastBle)
3. [zhihu/Matisse](https://github.com/zhihu/Matisse)
